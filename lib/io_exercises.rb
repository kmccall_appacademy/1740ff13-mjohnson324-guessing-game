# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a choice between
#   1 and 100. Prompt the user to `guess a choice`. Each time through a play loop,
#   get a guess from the user. Print the choice guessed and whether it was `too
#   high` or `too low`. Track the choice of guesses the player takes. When the
#   player guesses the choice, print out what the choice was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random choice using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  answer = ("1".."100").to_a.shuffle[0]
  puts "Guess a number:"
  number = gets.chomp
  guesses = 1

  until number == answer
    number, guesses = get_it_right(answer, number, guesses)
  end

  puts number
  puts "#{guesses} guessses"
end

def get_it_right(answer, choice, guess)
  puts "#{guess} guesses"
  puts "you last guessed #{choice}"
  if choice.to_i > answer.to_i
    puts "too high"
  else
    puts "too low"
  end
  puts "Guess a number:"

  choice = gets.chomp
  guess += 1
  [choice, guess]
end
